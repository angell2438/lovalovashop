<div class="wrapper-breadcrumb">
    <div class="container">
        <div class="row">
            <ul  class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
                        <?php } else { ?><span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>


