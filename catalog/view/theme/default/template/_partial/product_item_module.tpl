<div class="product-layout col-lg-3 col-md-3 col-sm-6  col-xs-6 col-xxs-12">
    <div class="product-thumb transition">
        <div class="image">
            <div class="button-group">
                <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-bag"></i></button>
                <button type="button"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');"><i class="icon-heart"></i></button>
                <a href="<?php echo $product['popup']; ?>" class="in_product"><i class="icon-search"></i></a>
            </div>
            <a href="<?php echo $product['href']; ?>">
                <img
                        src="<?php echo $product['thumb']; ?>"
                        alt="<?php echo $product['name']; ?>"
                        title="<?php echo $product['name']; ?>"
                        class="img-responsive" />
            </a>
        </div>
        <div class="caption">
            <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
            <?php if ($product['price']) { ?>
                <p class="price">
                    <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                    <?php } else { ?>
                        <span class="price-old"><?php echo $product['price']; ?></span><span class="price-new"><?php echo $product['special']; ?></span>
                    <?php } ?>
                </p>
            <?php } ?>
        </div>

    </div>
</div>