<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
        <div class="row">

            <h1><?php echo $heading_title; ?></h1>
            <div class="row">
                <div class="col-md-6">
                    <div class="item-contact">
                        <address>
                            <p class="style_addres start-adress">
                                <?php echo $address; ?></p>
                        </address>
                        <?php if ($open) { ?>
                            <p>
                                <?php echo $open; ?></p>
                            <br />
                        <?php } ?>
                        <?php if ($telephone) { ?>
                            <p>
                                <i class="icon-phone-call"></i>
                                <a  href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone ) ;?>"><?php echo $telephone; ?></a>
                            </p>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal form-style">
                        <div class="row">
                            <div class="col-md-6 col-sm-12 required">
                                <input type="text" name="name" value="<?php echo $name; ?>" id="input-name_contact" class="form-control" placeholder="<?php echo $entry_name; ?>"/>
                                <?php if ($error_name) { ?>
                                    <div class="text-danger"><?php echo $error_name; ?></div>
                                <?php } ?>

                            </div>
                            <div class="col-md-6 col-sm-12 required">
                                <input type="text"  name="email" value="<?php echo $email; ?>" id="input-email_contact" class="form-control" placeholder="<?php echo $entry_email; ?>"/>
                                <?php if ($error_email) { ?>
                                    <div class="text-danger"><?php echo $error_email; ?></div>
                                <?php } ?>

                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group required">

                                <textarea name="enquiry" rows="10" id="input-enquiry_contact" class="form-control" placeholder="<?php echo $entry_enquiry; ?>"><?php echo $enquiry; ?></textarea>
                                <?php if ($error_enquiry) { ?>
                                    <div class="text-danger"><?php echo $error_enquiry; ?></div>
                                <?php } ?>

                            </div>
                        </div>
                        <?php echo $captcha; ?>
                        <div class="buttons">
                            <div class="pull-right">
                                <input class="btn btn-primary" type="submit" value="<?php echo $button_submit; ?>" />
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <?php echo $content_top; ?>
        </div>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyALGbIxt_aCHqxFzSPX-KopFPL5BvkRsHg&extension=.js"></script>
        <script src="https://cdn.mapkit.io/v1/infobox.js"></script>
        <link href="https://cdn.mapkit.io/v1/infobox.css" rel="stylesheet" >

        <script>
            google.maps.event.addDomListener(window, 'load', init);
            var openedBox = null;
            var map, markersArray = [];

            function bindInfoWindow(marker, map, location) {
                google.maps.event.addListener(marker, 'click', function() {
                    function close(location) {
                        location.ib.close();
                        location.infoWindowVisible = false;
                        location.ib = null;
                    }

                    if (location.infoWindowVisible === true) {
                        close(location);
                    } else {
//                            markersArray.forEach(function(loc, index){
//                                if (loc.ib && loc.ib !== null) {
//                                    close(loc);
//                                }
//                            });

                        var boxText = document.createElement('div');
                        boxText.style.cssText = 'background: #fff;';
                        boxText.classList.add('md-whiteframe-2dp');

                        function buildPieces(location, el, part, icon) {
                            if (location[part] === '') {
                                return '';
                            } else if (location.iw[part]) {
                                switch(el){
                                    case 'photo':
                                        if (location.photo){
                                            return '<div class="iw-photo" style="background-image: url(' + location.photo + ');"></div>';
                                        } else {
                                            return '';
                                        }
                                        break;
                                    case 'iw-toolbar':
                                        return '<div class="iw-toolbar"><h3 class="md-subhead"><i class="icon_new-map-marker" style="color:#cab383;"></i>' + location.title + '</h3></div>';
                                        break;
                                    case 'div':
                                        switch(part){
                                            case 'address':
                                                return '<div class="iw-details adress"><span>' + location.address + '</span></div>';
                                                break;
                                            case 'email':
                                                return '<div class="iw-details"><span><a href="mailto:' + location.email + '" target="_blank">' + location.email + '</a></span></div>';
                                                break;
                                            case 'tel':
                                                return '<div class="iw-details"><span><a href="tel:' + location.tel + '" target="_blank">' + location.tel + '</a></span></div>';
                                                break;
                                            case 'int_tel':
                                                return '<div class="iw-details"><span><a href="tel:' + location.int_tel + '" target="_blank">' + location.int_tel + '</a></span></div>';
                                                break;
                                            case 'web':
                                                return '<div class="iw-details"><span><a href="' + location.web + '" target="_blank">' + location.web_formatted + '</a></span></div>';
                                                break;
                                            case 'desc':
                                                return '<label class="iw-desc" for="cb_details"><input type="checkbox" id="cb_details"/><h3 class="iw-x-details">Details</h3><p class="iw-x-details">' + location.desc + '</p></label>';
                                                break;
                                            default:
                                                return '<div class="iw-details"><span>' + location[part] + '</span></div>';
                                                break;
                                        }
                                        break;
                                    case 'open_hours':
                                        var items = '';
                                        if (location.open_hours.length > 0){
                                            for (var i = 0; i < location.open_hours.length; ++i) {
                                                if (i !== 0){
                                                    items += '<li><strong>' + location.open_hours[i].day + '</strong><strong>' + location.open_hours[i].hours +'</strong></li>';
                                                }
                                                var first = '<li><label for="cb_hours"><input type="checkbox" id="cb_hours"/><strong>' + location.open_hours[0].day + '</strong><strong>' + location.open_hours[0].hours +'</strong><i class="material-icons toggle-open-hours"><img src="//cdn.mapkit.io/v1/icons/keyboard_arrow_down.svg"/></i><ul>' + items + '</ul></label></li>';
                                            }
                                            return '<div class="iw-list"><i class="material-icons first-material-icons" style="color:#4285f4;"><img src="//cdn.mapkit.io/v1/icons/' + icon + '.svg"/></i><ul>' + first + '</ul></div>';
                                        } else {
                                            return '';
                                        }
                                        break;
                                }
                            } else {
                                return '';
                            }
                        }

                        boxText.innerHTML =
                            buildPieces(location, 'photo', 'photo', '') +
                            buildPieces(location, 'iw-toolbar', 'title', '') +
                            buildPieces(location, 'div', 'address', 'location_on') +
                            buildPieces(location, 'div', 'web', 'public') +
                            buildPieces(location, 'div', 'email', 'email') +
                            buildPieces(location, 'div', 'tel', 'phone') +
                            buildPieces(location, 'div', 'int_tel', 'phone') +
                            buildPieces(location, 'open_hours', 'open_hours', 'access_time') +
                            buildPieces(location, 'div', 'desc', 'keyboard_arrow_down');

                        var myOptions = {
                            alignBottom: true,
                            content: boxText,
                            disableAutoPan: true,
                            maxWidth: 0,
                            pixelOffset: new google.maps.Size(-140, -40),
                            zIndex: null,
                            boxStyle: {
                                opacity: 1,
                                width: '280px'
                            },
                            closeBoxMargin: '0px 0px 0px 0px',
                            infoBoxClearance: new google.maps.Size(1, 1),
                            isHidden: false,
                            pane: 'floatPane',
                            enableEventPropagation: false
                        };
                        if(openedBox && openedBox.close) openedBox.close();
                        location.ib = openedBox = new InfoBox(myOptions);
                        location.ib.open(map, marker);
                        location.infoWindowVisible = true;
                    }
                });
            }

            function init() {
                var mapOptions = {
                    center: new google.maps.LatLng(50.4187129,30.6349596),
                    zoom: 10,
                    gestureHandling: 'auto',
                    fullscreenControl: false,
                    zoomControl: true,
                    disableDoubleClickZoom: true,
                    mapTypeControl: true,
                    mapTypeControlOptions: {
                        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    },
                    scaleControl: true,
                    scrollwheel: true,
                    streetViewControl: true,
                    draggable : true,
                    clickableIcons: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    styles: [{"stylers":[{"saturation":-100},{"gamma":1}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.text","stylers":[{"visibility":"off"}]},{"featureType":"poi.place_of_worship","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"water","stylers":[{"visibility":"on"},{"saturation":50},{"gamma":0},{"hue":"#50a5d1"}]},{"featureType":"administrative.neighborhood","elementType":"labels.text.fill","stylers":[{"color":"#333333"}]},{"featureType":"road.local","elementType":"labels.text","stylers":[{"weight":0.5},{"color":"#333333"}]},{"featureType":"transit.station","elementType":"labels.icon","stylers":[{"gamma":1},{"saturation":50}]}]
                }
                var mapElement = document.getElementById('mapkit-9205');
                var map = new google.maps.Map(mapElement, mapOptions);
                var locations = [
                    {"title":"Салон 1","address":"ул. Салютная, 2Б","desc":"","tel":"(044)232-4-234","int_tel":"(067)234-234-2","email":"","web":"","web_formatted":"","open":"","time":"Режим работы <br/>8:00-22:00","lat":50.471262,"lng":30.399668,"vicinity":"","open_hours":"","marker":{"url":"/catalog/view/theme/default/image/marker2.png","scaledSize":{"width":55,"height":74,"f":"px","b":"px"},"origin":{"x":0,"y":0},"anchor":{"x":12,"y":42}},"iw":{"address":true,"desc":true,"email":false,"enable":true,"int_tel":true,"open":true,"open_hours":true,"photo":true,"tel":true,"title":true,"web":true}},
                    {"title":"Салон 2","address":"ул. Драгоманова, 2","desc":"","tel":"(044)232-4-234","int_tel":"(067)234-234-2","email":"","web":"","web_formatted":"","open":"","time":"Режим работы <br/>8:00-22:00","lat":50.4187129,"lng":30.6349596,"vicinity":"","open_hours":"","marker":{"url":"/catalog/view/theme/default/image/marker2.png","scaledSize":{"width":55,"height":74,"f":"px","b":"px"},"origin":{"x":0,"y":0},"anchor":{"x":12,"y":42}},"iw":{"address":true,"desc":true,"email":false,"enable":true,"int_tel":true,"open":true,"open_hours":true,"photo":true,"tel":true,"title":true,"web":true}},
                    {"title":"Салон 3","address":"ул. Святошинская, 27-а","desc":"","tel":"(044)232-4-234","int_tel":"(067)234-234-2","email":"","web":"","web_formatted":"","open":"","time":"Режим работы <br/>8:00-22:00","lat":50.3895535,"lng":30.3673806,"vicinity":"","open_hours":"","marker":{"url":"/catalog/view/theme/default/image/marker2.png","scaledSize":{"width":55,"height":74,"f":"px","b":"px"},"origin":{"x":0,"y":0},"anchor":{"x":12,"y":42}},"iw":{"address":true,"desc":true,"email":false,"enable":true,"int_tel":true,"open":true,"open_hours":true,"photo":true,"tel":true,"title":true,"web":true}},

                ];
                for (i = 0; i < locations.length; i++) {
                    var marker = new google.maps.Marker({
                        icon: locations[i].marker,
                        position: new google.maps.LatLng(locations[i].lat, locations[i].lng),
                        map: map,
                        title: locations[i].title,
                        address: locations[i].address,
                        desc: locations[i].desc,
                        tel: locations[i].tel,
                        int_tel: locations[i].int_tel,
                        vicinity: locations[i].vicinity,
                        open: locations[i].open,
                        open_hours: locations[i].open_hours,
                        photo: locations[i].photo,
                        time: locations[i].time,
                        email: locations[i].email,
                        web: locations[i].web,
                        iw: locations[i].iw
                    });
                    markersArray.push(marker);

                    if (locations[i].iw.enable === true){
                        bindInfoWindow(marker, map, locations[i]);
                    }
                }
                // map.addListener('center_changed', function() {
                //     // 3 seconds after the center of the map has changed, pan back to the
                //     // marker.
                //     window.setTimeout(function() {
                //         map.panTo(marker.getPosition());
                //     }, 3000);
                // });
                //
                // marker.addListener('click', function() {
                //     map.setZoom(10);
                //     map.setCenter(marker.getPosition());
                // });
                // google.maps.event.addListener(marker, 'click', function() {
                //     hideAllInfoWindows(map);
                //     this.bindInfoWindow.open(map, this);
                // });
            }
            // function hideAllInfoWindows(map) {
            //     markersArray.forEach(function(marker) {
            //         marker.bindInfoWindow.close(map, marker);
            //     });
            // }
        </script>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<div id='mapkit-9205' class="map"></div>
<?php echo $footer; ?>
