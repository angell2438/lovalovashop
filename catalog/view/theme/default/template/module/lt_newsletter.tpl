<div class="row" id="newsletter">
	<div class="col-sm-12">
			<div class="newsletter-form">
				<div><?php echo $text_before; ?></div>
				<form id="lt_newsletter_form">
					<div class="form-group">
						<div class="newsletter_form-input">
							<input type="email" required name="lt_newsletter_email" id="lt_newsletter_email" class="form-control input-lg" placeholder="<?php echo $entry_email; ?>">
						</div>
						<div class="input-group-btn">
								<button type="submit" class="btn btn-newsletter btn-default"><i class="icon-rectangle-2-copy-3"></i></button>
						</div>
					</div>
				</form>
	</div>
	</div>
</div>
<script type="text/javascript"><!--
		$(document).ready(function($) {
			$('#lt_newsletter_form').submit(function(){
				$.ajax({
					type: 'post',
					url: '<?php echo $action; ?>',
					data:$("#lt_newsletter_form").serialize(),
					dataType: 'json',
					beforeSend: function() {
						$('.btn-newsletter').attr('disabled', true).button('loading');
					},
					complete: function() {
						$('.btn-newsletter').attr('disabled', false).button('reset');
					},
					success: function(json) {
						$('.alert, .text-danger').remove();
						$('.form-group').removeClass('has-error');

						if (json.error) {
							$('#lt_newsletter_form').after('<div class="alert alert-danger newsletter-msg">' + json.error + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
						} else {
							$('#lt_newsletter_form').after('<div class="alert alert-success newsletter-msg">' + json.success + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
							$('#lt_newsletter_email').val('');
						}
					}

				});
				return false;
			});
		});
	//--></script>
