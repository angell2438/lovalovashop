<div id="banner<?php echo $module; ?>" class="banner-wrapper">
    <div class="container">
        <div class="row">
            <div class="title-wrapper">
                <h2>гарячі пропозиції</h2>
                <div class="wrapper-heart">
                    <span></span>
                    <i class="icon-heart"></i>
                    <span></span>
                </div>

            </div>
            <?php $k=""; foreach ($banners as $banner) { $k++;?>
                <div class="item col-md-6 col-sm-12">
                    <?php if ($banner['link']) { ?>
                        <div class="wrapper-item ">
                            <div class="item-text <?php if ($k==1) { ?> item-top <?php } ?> <?php if ($k==2) { ?> item-left <?php } ?> <?php if ($k==3) { ?> item-right <?php } ?>">
                                <?php if ($banner['title']) { ?>
                                    <p class="title-banner"><?php echo $banner['title']; ?></p>
                                <?php } ?>
                                <?php if ($banner['text']) { ?>
                                    <p class="name-txt"><?php echo $banner['text']; ?></p>
                                <?php } ?>
                                <?php if ($banner['text2']) { ?>
                                    <p class="price-txt"><?php echo $banner['text2']; ?></p>
                                <?php } ?>
                            </div>
                            <a href="<?php echo $banner['link']; ?>">
                                <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                            </a>
                        </div>

                    <?php } else { ?>
                        <div class="wrapper-item">
                            <div class="item-text <?php if ($k==1) { ?> item-top <?php } ?> <?php if ($k==2) { ?> item-left <?php } ?> <?php if ($k==3) { ?> item-right <?php } ?>">
                                <?php if ($banner['title']) { ?>
                                    <p class="title-banner"><?php echo $banner['title']; ?></p>
                                <?php } ?>
                                <?php if ($banner['text']) { ?>
                                    <p class="name-txt"><?php echo $banner['text']; ?></p>
                                <?php } ?>
                                <?php if ($banner['text2']) { ?>
                                    <p class="price-txt"><?php echo $banner['text2']; ?></p>
                                <?php } ?>
                            </div>
                            <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
