<div class="title-wrapper">
    <h2><?php echo $heading_title; ?></h2>
    <div class="wrapper-heart">
        <span></span>
        <i class="icon-heart"></i>
        <span></span>
    </div>
</div>
<div class="row">
    <?php foreach ($products as $product) { ?>
        <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>

    <?php } ?>
</div>