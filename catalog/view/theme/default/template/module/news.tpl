<div class="wrapper-new">
    <div class="container">
        <div class="row">
            <div class="title-wrapper">
                <h2><?php echo $heading_title; ?></h2>
                <div class="wrapper-heart">
                    <span></span>
                    <i class="icon-heart"></i>
                    <span></span>
                </div>
            </div>
            <div class="box-content">
                <div class="bnews-list<?php if ($display_style) { ?> bnews-list-2<?php } ?>">
                    <?php foreach ($article as $articles) { ?>
                        <div class="news-block js-height col-lg-3 col-md-3 col-sm-6  col-xs-6 col-xxs-12">
                            <div class="position-date">
                                <a class="" href="<?php echo $articles['href']; ?>">
                                    <?php if ($articles['thumb']) { ?>
                                        <img class="article-image" src="<?php echo $articles['thumb']; ?>" title="<?php echo $articles['name']; ?>" alt="<?php echo $articles['name']; ?>" />
                                    <?php } ?>
                                </a>
                            </div>
                            <a class="" href="<?php echo $articles['href']; ?>">
                                <?php if ($articles['date_added']) { ?>
                                    <span><?php echo $articles['date_added']; ?></span>
                                <?php } ?>
                                <p class="name-news">
                                    <?php if ($articles['name']) { ?>
                                        <?php echo $articles['name']; ?>
                                    <?php } ?>

                                </p>
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <div class="news_block_btn">
                <a class="btn-default" href="<?php echo $newslink; ?>"><?php echo $text_headlines; ?></a>
            </div>

        </div>
    </div>
</div>

<script ><!--
	$(document).ready(function() {
		$('img.article-image').each(function(index, element) {
		var articleWidth = $(this).parent().parent().width() * 0.7;
		var imageWidth = $(this).width() + 10;
		if (imageWidth >= articleWidth) {
			$(this).attr("align","center");
			$(this).parent().addClass('bigimagein');
		}
		});
	});
//--></script>
<?php if ($disqus_status) { ?>
<script >
var disqus_shortname = '<?php echo $disqus_sname; ?>';
(function () {
var s = document.createElement('script'); s.async = true;
s.type = 'text/javascript';
s.src = 'http://' + disqus_shortname + '.disqus.com/count.js';
(document.getElementsByTagName('HEAD')[0] || document.getElementsByTagName('BODY')[0]).appendChild(s);
}());
</script>
<?php } ?>
<?php if ($fbcom_status) { ?>
<script >
      window.fbAsyncInit = function() {
        FB.init({
          appId      : '<?php echo $fbcom_appid; ?>',
		  status     : true,
          xfbml      : true,
		  version    : 'v2.0'
        });
      };

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
</script>
<?php } ?>