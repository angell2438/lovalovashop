<div class="wrapper-prod_home">
    <div class="container">
        <div class="row">
            <div class="title-wrapper">
                <h2>наші продукти</h2>
                <div class="wrapper-heart">
                    <span></span>
                    <i class="icon-heart"></i>
                    <span></span>
                </div>
            </div>
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#panel1">найпопулярніші</a></li>
                <li><a data-toggle="tab" href="#panel2">найцікавіші</a></li>
                <li><a data-toggle="tab" href="#panel3">акції та новинки</a></li>
            </ul>

            <div class="tab-content">
                <div id="panel1" class="tab-pane fade in active">
                    <?php foreach ($products_populars as $product) { ?>
                        <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist));?>
                    <?php } ?>
                </div>
                <div id="panel2" class="tab-pane fade">
                    <?php foreach ($products_interesting as $product) { ?>
                        <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist));?>
                    <?php } ?>
                </div>
                <div id="panel3" class="tab-pane fade">
                    <?php foreach ($products_sales as $product) { ?>
                        <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist));?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>

