<div class="html-block">
    <div class="container">
        <div class="row">
            <?php if($heading_title) { ?>
                <h2><?php echo $heading_title; ?></h2>
            <?php } ?>
            <?php echo $html; ?>
        </div>
    </div>

</div>
