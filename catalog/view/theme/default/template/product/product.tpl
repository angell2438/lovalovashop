<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="prod-container">
    <div class="container" >

        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <div class="row">
                    <?php if ($column_left || $column_right) { ?>
                        <?php $class = 'col-sm-6'; ?>
                    <?php } else { ?>
                        <?php $class = 'col-sm-6'; ?>
                    <?php } ?>
                    <div class="<?php echo $class; ?>">
                        <?php if ($thumb || $images) { ?>
                            <?php if(count($images) >= 1) { ?>
                                <div class="slider-image-big-curier">
                                    <div class="col-md-12">
                                        <div class="thumbnails js-height" id="preview_imgs">
                                            <div class="item thumbnails-pop">
                                                <?php if ($news) { ?>
                                                    <div class="new-product">
                                                        <p>NEW</p>
                                                    </div>
                                                <?php } ?>
                                                <a  href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                                                    <img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                                </a>
                                            </div>
                                            <?php $k = '';
                                            foreach ($images as $image) {
                                                $k++; ?>
                                                <div class="item thumbnails-pop">
                                                    <?php if ($news) { ?>
                                                        <div class="new-product">
                                                            <p>NEW</p>
                                                        </div>
                                                    <?php } ?>
                                                    <a href="<?php echo $image['popup']; ?>">
                                                        <img itemprop="image" src="<?php echo $image['thumb']; ?>"
                                                             data-link="image-<?php echo $k; ?>"
                                                             class="img-responsive"
                                                             title="<?php echo $heading_title; ?>"
                                                             alt="<?php echo $heading_title; ?>"/>
                                                    </a>
                                                </div>

                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="thumbnails-mini" id="thumbnail_imgs">
                                            <div class="item ">
                                                <img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                            </div>
                                            <?php $k = '';
                                            foreach ($images as $image) {
                                                $k++; ?>
                                                <?php if ($image): ?>
                                                    <div class="item ">
                                                        <img itemprop="image" src="<?php echo $image['thumb']; ?>"
                                                             data-link="image-<?php echo $k; ?>"
                                                             class="img-responsive"
                                                             title="<?php echo $heading_title; ?>"
                                                             alt="<?php echo $heading_title; ?>"/>
                                                    </div>
                                                <?php endif;  ?>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </div>
                                <ul id="lightgallery" style="display: none">
                                    <li data-src="<?php echo $popup; ?>" id="image-0">
                                        <a href="">
                                            <img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>">
                                        </a>
                                    </li>
                                    <?php $k = '';
                                    foreach ($images as $image) {
                                        $k++; ?>

                                        <li data-src="<?php echo $image['popup']; ?>" id="image-<?php echo $k; ?>">
                                            <a href="<?php echo $image['popup']; ?>">
                                                <img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>">
                                            </a>
                                        </li>

                                    <?php } ?>
                                </ul>
                            <?php } else { ?>
                                <?php if ($thumb) { ?>
                                    <div class="slider-image-big js-height">
                                        <div class="item thumbnails-pop">
                                            <a  class="pop-img" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                                                <img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                            </a>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>


                    </div>
                    <?php if ($column_left || $column_right) { ?>
                        <?php $class = 'col-sm-6'; ?>
                    <?php } else { ?>
                        <?php $class = 'col-sm-6'; ?>
                    <?php } ?>
                    <div class="<?php echo $class; ?>">

                        <h1><?php echo $heading_title; ?></h1>
                        <div class="info-prod">

                                <?php if ($code_prod) { ?>
                                    <div class="item-info"><?php echo $text_code_prod; ?> <span><?php echo $code_prod; ?></span></div>
                                <?php } ?>

                            <div class="item-info"><?php echo $text_stock; ?> <span><?php echo $stock; ?></span></div>
                        </div>
                        <div class="info-prod">
                            <div class="item-info">
                                <?php if ($price) { ?>
                                    <p class="price price-cart">
                                        <?php if (!$special) { ?>
                                            <?php echo $price; ?>
                                        <?php } else { ?>
                                            <span class="price-old"><?php echo $price; ?></span><span class="price-new"><?php echo $special; ?></span>
                                        <?php } ?>
                                    </p>
                                <?php } ?>
                            </div>
                            <?php if (0) { ?>
                                <div class="item-info">
                                    <?php if ($review_status) { ?>
                                        <div class="rating">
                                            <p>
                                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                    <?php if ($rating < $i) { ?>
                                                        <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    <?php } else { ?>
                                                        <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                    <?php } ?>
                                                <?php } ?>
                                                <span class="fa-stack"><?php echo $reviews; ?></span>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                        </div>
                        <div id="product" class="container-options">
                            <?php if (isset($unit_products) and count($unit_products) > 1) { ?>
                                <div class="card-info__item form-group">
                                    <p class="card-info__val">Цвет</p>
                                    <div id="input-option-<?php echo $product_id; ?>" class="color">
                                        <?php foreach ($unit_products as $unit_product) { ?>
                                            <div class="color-radio">
                                                <a class="color-radio__btn" href="<?php echo $unit_product['href']; ?>">
                                                    <img src="<?php echo $unit_product['option_image']; ?>" alt="<?php echo $unit_product['name']; ?>"/>
                                                </a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } else { ?>

                            <?php } ?>
                            <?php if ($options) { ?>
                                <?php foreach ($options as $option) { ?>
                                    <?php if ($option['type'] == 'select') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                                <option value=""><?php echo $text_select; ?></option>
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                        <?php if ($option_value['price']) { ?>
                                                            (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                        <?php } ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'radio') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                            <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'checkbox') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                            <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'image') { ?>
                                        <div class="hidden form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                    <div class="radio">
                                                        <label>
                                                            <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" checked/>
                                                            <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </label>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'text') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'textarea') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'file') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label"><?php echo $option['name']; ?></label>
                                            <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                            <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'date') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group date">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'datetime') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group datetime">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'time') { ?>
                                        <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                            <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                            <div class="input-group time">
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                            <?php if ($minimum > 1) { ?>
                                <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                            <?php } ?>
                            <div class="btn-flex">
                                <div class="qty_block">
                                    <div class="wrapp_inputs">
                                        <div class="button button_minus">-</div>
                                        <input type="text" name="quantity" value="<?php echo $minimum; ?>" data-min="<?php echo $minimum; ?>" size="2" id="input-quantity"/>
                                        <div class="button button_plus">+</div>
                                    </div>
                                    <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                </div>
                                <div class="buy_block">
                                    <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>">
                            <span>
                                <span><?php echo $button_cart; ?></span>
                            </span>

                                    </button>
                                </div>
                                <button type="button" data-toggle="tooltip" class="in_wishlist wishlist-js <?php echo $in_wishlist? 'disabled': '' ?>" <?php echo $in_wishlist? 'disabled': '' ?> onclick="wishlist.add('<?php echo $product_id; ?>');">
                         <span>
                            <span><i class="fa fa-heart-o" aria-hidden="true"></i></span>
                         </span>
                                </button>
                            </div>

                        </div>
                        <div class="container-description-prod">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
                                <?php if ($attribute_groups) { ?>
                                    <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                                <?php } ?>

                                <?php if ($instruction) { ?>
                                    <li><a href="#tab-instruction" data-toggle="tab"><?php echo $tab_instruction; ?></a></li>
                                <?php } ?>
<!--                                --><?php //if (0) { ?>
                                    <?php if ($review_status) { ?>
                                        <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
                                    <?php } ?>
                                <?php if ($manufacturer) { ?>
                                    <li><a href="#tab-manufacturer" data-toggle="tab">Про бренд</a></li>
                                <?php } ?>
<!--                                --><?php //} ?>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
                                <?php if ($attribute_groups) { ?>
                                    <div class="tab-pane" id="tab-specification">
                                        <table class="table table-bordered">
                                            <?php foreach ($attribute_groups as $attribute_group) { ?>
                                                <tbody>
                                                <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                    <tr>
                                                        <td><?php echo $attribute['name']; ?></td>
                                                        <td><?php echo $attribute['text']; ?></td>
                                                    </tr>
                                                <?php } ?>
                                                </tbody>
                                            <?php } ?>
                                        </table>
                                    </div>
                                <?php } ?>
                                <?php if ($instruction) { ?>
                                    <div class="tab-pane" id="tab-instruction"><?php echo $instruction; ?></div>
                                <?php } ?>
<!--                                --><?php //if (0) { ?>
                                     <?php if ($review_status) { ?>
                                    <div class="tab-pane" id="tab-review">
                                        <form class="form-horizontal" id="form-review">
                                            <div id="review"></div>
                                            <h2><?php echo $text_write; ?></h2>
                                            <?php if ($review_guest) { ?>
                                                <div class="form-group required">
                                                    <div class="col-sm-12">
                                                        <label class="control-label" for="input-namereview"><?php echo $entry_name; ?></label>
                                                        <input type="text" name="name" value="" id="input-namereview" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    <div class="col-sm-12">
                                                        <label class="control-label" for="input-review"><?php echo $entry_review; ?></label>
                                                        <textarea name="text" rows="5" id="input-review" class="form-control"></textarea>

                                                    </div>
                                                </div>
                                                <div class="form-group required hidden">
                                                    <div class="col-sm-12">
                                                        <label class="control-label"><?php echo $entry_rating; ?></label>
                                                        &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                                                        <input type="radio" name="rating" value="1" />
                                                        &nbsp;
                                                        <input type="radio" name="rating" value="2" />
                                                        &nbsp;
                                                        <input checked type="radio" name="rating" value="3" />
                                                        &nbsp;
                                                        <input type="radio" name="rating" value="4" />
                                                        &nbsp;
                                                        <input type="radio" name="rating" value="5" />
                                                        &nbsp;<?php echo $entry_good; ?></div>
                                                </div>
                                                <?php echo $captcha; ?>
                                                <div class="buttons clearfix">
                                                    <div class="pull-right">
                                                        <button type="button" id="button-review" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-primary"><?php echo $button_continue; ?></button>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <?php echo $text_login; ?>
                                            <?php } ?>
                                        </form>
                                    </div>
                                <?php } ?>
                                <?php if ($manufacturer) { ?>
                                    <div class="tab-pane" id="tab-manufacturer"><?php echo $description_manufacturer; ?></div>
                                <?php } ?>
<!--                                --><?php //} ?>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <?php echo $column_right; ?></div>
    </div>
</div>
<?php if ($products) { ?>
<div class="related-prod">
    <div class="container">
        <div class="row">

                <div class="title-wrapper">
                    <h2><?php echo $text_related; ?></h2>
                    <div class="wrapper-heart">
                        <span></span>
                        <i class="icon-heart"></i>
                        <span></span>
                    </div>

                </div>
                <div class="row">
                    <div class="carousel-wrapper__releted">
                        <?php foreach ($products as $product) { ?>
                            <?php $this->partial('product_item_module', array('product' => $product, 'button_cart' => $button_cart, 'text_tax' => $text_tax, 'button_wishlist' => $button_wishlist, 'button_compare' => $button_compare));?>
                        <?php } ?>
                    </div>
                </div>

        </div>
    </div>
</div>
<?php } ?>
<div class="wrapper-module">
    <div class="container">
        <div class="row">
            <?php echo $content_bottom; ?>
        </div>
    </div>
</div>


<script ><!--
$('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
	$.ajax({
		url: 'index.php?route=product/product/getRecurringDescription',
		type: 'post',
		data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
		dataType: 'json',
		beforeSend: function() {
			$('#recurring-description').html('');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();

			if (json['success']) {
				$('#recurring-description').html(json['success']);
			}
		}
	});
});
//--></script>
<script ><!--
$('#button-cart').on('click', function() {
	$.ajax({
		url: 'index.php?route=checkout/cart/add',
		type: 'post',
		data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
		dataType: 'json',
		beforeSend: function() {
			$('#button-cart').button('loading');
		},
		complete: function() {
			$('#button-cart').button('reset');
		},
		success: function(json) {
			$('.alert, .text-danger').remove();
			$('.form-group').removeClass('has-error');

			if (json['error']) {
				if (json['error']['option']) {
					for (i in json['error']['option']) {
						var element = $('#input-option' + i.replace('_', '-'));

						if (element.parent().hasClass('input-group')) {
							element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						} else {
							element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
						}
					}
				}

				if (json['error']['recurring']) {
					$('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
				}

				// Highlight any found errors
				$('.text-danger').parent().addClass('has-error');
			}

			if (json['success']) {
                $('header').before('<div class="user_message_wrapp" style="opacity: 0"><div class="container alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div></div>');

                // $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                $('.user_message_wrapp').animate({
                    opacity: 1
                }, 300);
				$('#cart > button').html('<i class="icon-bag"></i><span id="cart-total"> ' + json['total'] + '</span>');

				$('html, body').animate({ scrollTop: 0 }, 'slow');

				$('#cart > ul').load('index.php?route=common/cart/info ul li');
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});
//--></script>
<script ><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});

$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;

	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=tool/upload',
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
//--></script>
<script ><!--
$('#review').delegate('.pagination a', 'click', function(e) {
    e.preventDefault();

    $('#review').fadeOut('slow');

    $('#review').load(this.href);

    $('#review').fadeIn('slow');
});

$('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

$('#button-review').on('click', function() {
	$.ajax({
		url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
		type: 'post',
		dataType: 'json',
		data: $("#form-review").serialize(),
		beforeSend: function() {
			$('#button-review').button('loading');
		},
		complete: function() {
			$('#button-review').button('reset');
		},
		success: function(json) {
			$('.alert-success, .alert-danger').remove();

			if (json['error']) {
				$('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
			}

			if (json['success']) {
				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').prop('checked', false);
			}
		}
	});
});

$(document).ready(function() {
	$('.thumbnails').magnificPopup({
		type:'image',
		delegate: 'a',
		gallery: {
			enabled:true
		}
	});
});
//--></script>
<?php echo $footer; ?>
