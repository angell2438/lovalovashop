<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">
  <div class="row">

    <div id="content" class="wrapper-xs-padding">
<!--        <div class="row">-->
            <h1><?php echo $heading_title; ?></h1>
            <?php echo $content_top; ?>
            <div class="content-information">
                <?php echo $description; ?>
            </div>
            <?php echo $content_bottom; ?>
<!--        </div>-->

    </div>
    </div>
</div>
<?php echo $footer; ?> 