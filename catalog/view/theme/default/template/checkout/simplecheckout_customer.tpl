<div class="simplecheckout-block" id="simplecheckout_customer" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $display_error && $has_error ? 'data-error="true"' : '' ?>>
  <?php if ($display_header || $display_login) { ?>
      <div class="title-wrapper">
          <h2><?php echo $text_checkout_customer;  ?></h2>
          <div class="wrapper-heart">
              <span></span>
              <i class="icon-heart"></i>
              <span></span>
          </div>
      </div>
  <?php } ?>
  <div class="simplecheckout-block-content">
      <div class="row">
          <?php if ($display_registered) { ?>
              <div class="success"><?php echo $text_account_created ?></div>
          <?php } ?>
          <?php if ($display_you_will_registered) { ?>
              <div class="you-will-be-registered"><?php echo $text_you_will_be_registered ?></div>
          <?php } ?>
          <?php foreach ($rows as $row) { ?>
              <?php echo $row ?>
          <?php } ?>
      </div>

  </div>
</div>