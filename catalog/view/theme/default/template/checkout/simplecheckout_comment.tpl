<div class="simplecheckout-block clearfix" id="simplecheckout_comment">
    <?php if ($display_header) { ?>
      <div class="checkout-heading panel-heading"><?php echo $label ?></div>
    <?php } ?>
    <div class="simplecheckout-block-content col-md-12">
      <textarea class="form-control" name="comment" id="comment" placeholder="<?php echo $placeholder ?>" data-reload-payment-form="true"><?php echo $comment ?></textarea>
    </div>
</div>