<div id="cart" class="btn-group btn-block cart-wrapper">
  <button
          type="button"
          data-toggle="dropdown"
          data-loading-text="<?php echo $text_loading; ?>"
          class="btn btn-cart dropdown-toggle">
      <i class="icon-bag"></i>
      <span id="cart-total"><?php echo $text_items; ?></span>
  </button>
  <ul class="dropdown-menu pull-right table-basket">
      <?php if ($products || $vouchers) { ?>
          <li>
              <!-- <a href="#request_call-modal">callback</a> -->
              <table class="table table-cart">
                  <tbody>
                  <?php foreach ($products as $product) { ?>
                      <tr class="cart-items clearfix">
                          <td class='wrapper-image'>
                              <?php if ($product['thumb']) { ?>
                                  <a  href="<?php echo $product['href']; ?>">
                                      <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>"  />
                                  </a>
                              <?php } ?>
                          </td>
                          <td class="wrapper-name">
                              <a class='cart-product' href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                          </td>
                          <td>
                              <div class="qty_block">
                                  x <?php echo $product['quantity']; ?>
<!--                                  <div class="wrapp_inputs">-->

<!--                                      <div class="button button_minus" data-key="--><?php //echo !empty($product['cart_id']) ? $product['cart_id'] : $product['key']; ?><!--">-</div>-->
<!--                                      <input type="text" name="quantity" value="--><?php //echo $product['quantity']; ?><!--"  size="2" id="input-quantity"/>-->
<!--                                      <div class="button button_plus" data-key="--><?php //echo !empty($product['cart_id']) ? $product['cart_id'] : $product['key']; ?><!--">+</div>-->
<!--                                  </div>-->
                                  <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                              </div>
                          </td>
                          <td>
                              <span class="price-cart"><?php echo $product['total']; ?></span>
                          </td>
                          <td>
                              <div class="container-btn">
                                  <button type="button" onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="btn-close delete-items">
                          <span class="wrapper-icon">
                              <span class="close-icon"></span>
                              <span class="close-icon"></span>
                          </span>
                                  </button>
                              </div>
                          </td>
                      </tr>
                  <?php } ?>
                  </tbody>
              </table>
              <?php foreach ($vouchers as $voucher) { ?>
                  <td class="text-center"></td>
                  <td class="text-left"><?php echo $voucher['description']; ?></td>
                  <td class="text-right">x&nbsp;1</td>
                  <td class="text-right"><?php echo $voucher['amount']; ?></td>
                  <td class="text-center text-danger"><button type="button" onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>
              <?php } ?>

          </li>
          <li>
              <div class="block-summ">
                  <table class="table table-cart table-cart_xs">
                      <?php foreach ($totals as $total) { ?>
                          <tr>
                              <td><p><?php echo $total['title']; ?></p></td>
                              <td><p class="price-size total"><?php echo $total['text']; ?></p></td>
                          </tr>
                      <?php } ?>
                  </table>
              </div>
              <div class="btn-wrapper clearfix">
                  <a class='btn btn-default' href="<?php echo $home; ?>">На головну</a>
                  <a class='btn btn-primary btn-checout' href="<?php echo $checkout; ?>"><?php echo $text_checkout; ?></a>
              </div>

          </li>
      <?php } else { ?>
          <li>
              <p class="text-center"><?php echo $text_empty; ?></p>
          </li>
      <?php } ?>
  </ul>
</div>
