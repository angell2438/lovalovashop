<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
<base href="<?php echo $base; ?>" />
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content= "<?php echo $keywords; ?>" />
<?php } ?>
<meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<?php if ($og_image) { ?>
<meta property="og:image" content="<?php echo $og_image; ?>" />
<?php } else { ?>
<meta property="og:image" content="<?php echo $logo; ?>" />
<?php } ?>
<meta property="og:site_name" content="<?php echo $name; ?>" />
    <?php if($gtm) { ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?
id='+i+dl;f.parentNode.insertBefore(j,f);
 })(window,document,'script','dataLayer','<?php echo $gtm; ?>');</script>
<!-- End Google Tag Manager -->
    <?php } ?>
<script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" ></script>
<link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
<script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js" ></script>
<link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="catalog/view/javascript/jquery/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="catalog/view/javascript/jquery/slick/slick-theme.css" rel="stylesheet">
    <link href="catalog/view/javascript/jquery/slick/slick.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/select2.min.css" rel="stylesheet">
<!--    <link href="catalog/view/theme/default/stylesheet/fonts.css" rel="stylesheet">-->
    <link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/sergo.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
<?php foreach ($styles as $style) { ?>
<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script src="catalog/view/javascript/common.js" ></script>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<?php foreach ($scripts as $script) { ?>
<script src="<?php echo $script; ?>" ></script>
<?php } ?>
<?php foreach ($analytics as $analytic) { ?>
<?php echo $analytic; ?>
<?php } ?>
</head>
<body class="<?php echo $class; ?>">
<?php if($gtm) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>>"
                      height="0" width="0"
                      style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } ?>
<div class="page-wrapper panel-overlay">
<nav id="top" class="top">
  <div class="container">
      <div class="row">
          <div class="col-md-10 col-xs-6">
              <div class="row">
                  <div class="wrapper-top-menu">
                      <div class="visible-xs">
                          <button class="open-xs-menu">
                              <i class="fa fa-bars"></i>
                          </button>
                      </div>
                      <ul class="list-inline">
                          <?php if ($home == $og_url) { ?>
                              <li><?php echo $text_home; ?></li>
                          <?php } else { ?>
                              <li><a href="<?php echo $home; ?>"><?php echo $text_home; ?> </a></li>
                          <?php } ?>
                          <?php if ($informations) { ?>
                              <?php foreach ($informations as $information) { ?>
                                  <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                              <?php } ?>
                          <?php } ?>

                          <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                      </ul>
                  </div>
              </div>

          </div>
          <div class="col-md-2 col-xs-6">
              <div class="row">
                      <div class="login-block dropdown">
                          <a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown">
                              <i class="icon-user"></i>
                              <span class="hidden-xs hidden-sm"><?php echo $text_account; ?></span>
                              <span class="fa fa-angle-down"></span>
                          </a>
                          <ul class="dropdown-menu dropdown-menu-right">
                              <?php if ($logged): ?>
                                  <li><a class="login" href="<?= $account ?>"><?php echo $text_account; ?></a></li>
                                  <li><a class="register" href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                              <?php else: ?>
                                  <li><a class="login" href="" data-toggle="modal" data-target="#modal-login"><?php echo $text_login; ?></a></li>
                                  <li><a class="register" href="" data-toggle="modal" data-target="#modal-register"><?php echo $text_register; ?></a></li>
                              <?php endif; ?>
                          </ul>
                      </div>

              </div>
          </div>

      </div>

  </div>
</nav>

<header>
  <div class="container">
    <div class="row">
        <div class="col-md-12 container-line">
            <div class="row">
                <div class="col-md-9 col-sm-6 col-xs-12">
                    <div class="row">
                        <a class="phone-shop" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone) ;?>">
                            <i class="icon-phone-call"></i>
                            <?php echo $telephone; ?>
                        </a>
                        <button class="btn btn-call-back" type="button" data-toggle="modal" data-target="#request_call-modal">зворотній дзвінок</button>
                    </div>

                </div>

                <div class="col-md-3 col-sm-6 col-xs-12 cont-margin">
                    <div class="row">
                        <div class="col-md-10 col-sm-10 col-xs-10">
                            <div class="row">
                                <?php echo $search; ?>
                            </div>

                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <div class="row">
                                <?php echo $cart; ?>
                            </div>

                        </div>
                    </div>


                </div>

            </div>
        </div>
      <div class="col-sm-12">
        <div class="logo">
          <?php if ($logo) { ?>
            <?php if ($home == $og_url) { ?>
              <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
            <?php } else { ?>
              <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
            <?php } ?>
          <?php } else { ?>
            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
          <?php } ?>
        </div>
      </div>


    </div>
  </div>
</header>
<?php if ($categories) { ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <nav id="menu" class="navbar">
                <div class="navbar-header">
                    <button type="button" class="btn btn-navbar navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span id="category" class="visible-xs"><?php echo $text_category; ?></span>
                        <i class="fa fa-angle-down"></i>
                    </button>
                </div>
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <?php foreach ($categories as $category) { ?>
                            <?php if ($category['children']) { ?>
                                <li class="dropdown">
                                    <a href="<?php echo $category['href']; ?>" class="dropdown-toggle" data-toggle="dropdown">
                                        <?php echo $category['name']; ?>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <div class="dropdown-menu">
                                        <div class="dropdown-inner">
                                            <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                                <ul class="list-unstyled">
                                                    <?php foreach ($children as $child) { ?>
                                                        <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                                    <?php } ?>
                                                </ul>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </li>
                            <?php } else { ?>
                                <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                            <?php } ?>
                        <?php } ?>
                    </ul>
                </div>
            </nav>
        </div>

    </div>

</div>
<?php } ?>
    <div class="main-content">
        <h1 class="hidden"><?php echo $name; ?></h1>
