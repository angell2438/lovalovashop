<div id="search" class="input-group search-group">
  <input
          type="text"
          name="search"
          value="<?php echo $search; ?>"
          placeholder="<?php echo $text_search; ?>"
          class="form-control input-lg" />
  <span class="input-group-btn">
    <button type="button" class="btn btn-lg">
        <i class="icon-search"></i>
    </button>
  </span>
</div>