</div>
<footer>
  <div class="container">
    <div class="row">
        <div class="col-lg-6 col-md-12">

                <h3>Про нас</h3>
                <div class="info-wrapper">
                    <p>
                        Интернет магазин MakeUp дает возможность заказать онлайн оригинальную косметику и парфюмерию.
                        Мы поможем вам подобрать необходимые средства по уходу, качественную декоративную косметику и эксклюзивные нишевые ароматы.
                    </p>
                    <a href="/about_us/">дізнатись більше ></a>
                </div>
                <div class="social_btn">
                    <?php if($config_fb) { ?>
                        <a href="<?php echo $config_fb; ?>" target="_blank"><i class="icon-facebook"></i></a>
                    <?php } ?>
                    <?php if($config_inst) { ?>
                        <a href="<?php echo $config_inst; ?>" target="_blank"><i class="icon-instagram"></i></a>
                    <?php } ?>
                    <?php if($config_vk) { ?>
                        <a href="<?php echo $config_vk; ?>" target="_blank"><i class="icon-vk"></i></a>
                    <?php } ?>
                </div>

        </div>
        <div class="col-lg-6 col-md-12 ">
            <div class="row not-row-md">
                <?php if ($informations) { ?>
                    <div class="col-lg-4 col-sm-4 col-xs-4 col-xxs-12">
                        <div class="row">
                            <h3><?php echo $text_service; ?></h3>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                                <?php foreach ($informations as $information) { ?>
                                    <li><a href="<?php echo $information['href']; ?>"><?php echo $information['title']; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>

                    </div>
                <?php } ?>
                <div class="col-lg-4 col-sm-4 col-xs-4 col-xxs-12">
                    <div class="wrapp-marg_right wrapp-marg_xs-left">
                        <h3><?php echo $text_extra; ?></h3>
                        <ul class="list-unstyled ">
                            <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
                            <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
                            <li><a href="<?php echo $affiliate; ?>"><?php echo $text_affiliate; ?></a></li>
                            <li><a href="<?php echo $special; ?>"><?php echo $text_specials; ?></a></li>
                            <li><a href="/sitemap.xml"><?php echo $text_sitemap; ?></a></li>
                        </ul>
                    </div>

                </div>
                <div class="col-lg-4 col-sm-4 col-xs-4 col-xxs-12">
                    <div class="wrapp-marg_xs-left">
                        <h3><?php echo $text_account; ?></h3>
                        <ul class="list-unstyled">
                            <?php if ($logged): ?>
                                <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                                <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
                                <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                                <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
                                <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                            <?php else: ?>
                                <li><a class="login" href="" data-toggle="modal" data-target="#modal-login"><?php echo $text_account; ?></a></li>
                                <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
                                <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
                            <?php endif; ?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
  </div>
    <div class="copiright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>
                        LovaLova © 2017
                    </p>
                </div>
                <div class="col-md-6">
                    <div class="img-wrap">
                        <img src="catalog/view/image/payment.png" alt="payment">
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<div class="back-top">
    <a href="#top"><span class="icon-next"></span></a>
</div>
<?php echo $quicksignup; ?>
<script src="catalog/view/javascript/jquery/fancybox/jquery.fancybox.pack.js"></script>
<script src="catalog/view/javascript/jquery/slick/slick.js"></script>
<script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
<script src="catalog/view/javascript/jquery/select2.min.js"></script>
<script src="catalog/view/javascript/jquery/machheigh.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script src="catalog/view/javascript/jquery/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lightgallery.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-fullscreen.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-thumbnail.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-video.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-autoplay.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-zoom.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-hash.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-pager.js"></script>
<script src="catalog/view/javascript/main.js"></script>
</body></html>