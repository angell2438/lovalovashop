<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>

<?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
<?php } ?>
<div class="container">

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>">
        <div class="row">
            <?php echo $content_top; ?>
            <div class="wrapper-margin">
                <h1><?php echo $heading_title; ?></h1>
                <div class="row">
                    <?php if ($products) { ?>
                        <?php foreach ($products as $product) { ?>
                            <div class="product-layout col-lg-3 col-md-3 col-sm-6  col-xs-6 col-xxs-12">
                                <div class="product-thumb transition">
                                    <div class="image">
                                        <div class="button-group">
                                            <button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-bag"></i></button>
                                            <a class="mini-btn" href="<?php echo $product['remove']; ?>" >×</a>
                                            <a href="<?php echo $product['popup']; ?>" class="in_product"><i class="icon-search"></i></a>
                                        </div>
                                        <a href="<?php echo $product['href']; ?>">
                                            <img
                                                    src="<?php echo $product['thumb']; ?>"
                                                    alt="<?php echo $product['name']; ?>"
                                                    title="<?php echo $product['name']; ?>"
                                                    class="img-responsive" />
                                        </a>
                                    </div>
                                    <div class="caption">
                                        <a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                        <?php if ($product['price']) { ?>
                                            <p class="price">
                                                <?php if (!$product['special']) { ?>
                                                    <?php echo $product['price']; ?>
                                                <?php } else { ?>
                                                    <span class="price-old"><?php echo $product['price']; ?></span><span class="price-new"><?php echo $product['special']; ?></span>
                                                <?php } ?>
                                            </p>
                                        <?php } ?>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                    <?php } else { ?>
                        <div class="col-xs-12">
                            <p><?php echo $text_empty; ?></p>
                            <div class="buttons clearfix">
                                <div class="pull-right">
                                    <a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_continue; ?></a>
                                </div>
                            </div>
                        </div>

                    <?php } ?>
                </div>
            </div>

            <?php echo $content_bottom; ?>
        </div>

    </div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>