<?php
class ControllerModuleProductHome extends Controller {
    public function index($setting) {
        $this->load->language('module/latest');

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_tax'] = $this->language->get('text_tax');

        $data['text_promotions'] = $this->language->get('text_promotions');
        $data['text_discounts'] = $this->language->get('text_discounts');
        $data['text_sales'] = $this->language->get('text_sales');

        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');
        $data['text_more_news'] = $this->language->get('text_more_news');
        $data['promotions'] = $this->url->link('product/category', 'path=64');
        $data['discounts'] = $this->url->link('product/category', 'path=65');
        $data['sales'] = $this->url->link('product/category', 'path=66');
        $data['button_sku'] = $this->language->get('button_sku');

        $populars = $this->config->get('config_popular');
        $most_interesting = $this->config->get('config_interesting');
        $sales = $this->config->get('config_sales');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        $data['products_populars'] = array();

        $filter_data = array(
            'filter_category_id' => (int)$populars,
            'start'           => 0,
            'limit'           => 8
        );
//        var_dump($filter_data);
        $results = $this->model_catalog_product->getProducts($filter_data);
//        var_dump($results);
        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                    $popup = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'), 'product_list', true);

                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                    $popup = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'), 'product_list', true);

                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                } else {
                    $tax = false;
                }

                if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
                    $wishlist_status = '1';
                } else {
                    $wishlist_status = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $result['rating'];
                } else {
                    $rating = false;
                }


                $data['products_populars'][] = array(
                    'product_id'  => $result['product_id'],
                    'thumb'       => $image,
                    'popup'       => $popup,
                    'name'        => $result['name'],
                    'wishlist_status' => $wishlist_status,
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'tax'         => $tax,
                    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                    'rating'      => $result['rating'],
                    'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                );
            }
//            var_dump($data['products_populars']);
        }


        $data['products_interesting'] = array();

        $filter_data = array(
            'filter_category_id' => (int)$most_interesting,
            'start'           => 0,
            'limit'           => 8
        );

        $results = $this->model_catalog_product->getProducts($filter_data);

        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                    $popup = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'), 'product_list', true);

                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                    $popup = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'), 'product_list', true);

                }


                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
                    $wishlist_status = '1';
                } else {
                    $wishlist_status = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $result['rating'];
                } else {
                    $rating = false;
                }

//                if ($result['model']=='3814') {
//                echo "<pre>";
//                var_dump($avaiblesizes);
//                }

                $data['products_interesting'][] = array(
                    'product_id'  => $result['product_id'],
                    'thumb'       => $image,
                    'popup'       => $popup,
                    'name'        => $result['name'],
                    'wishlist_status' => $wishlist_status,
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'tax'         => $tax,
                    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                    'rating'      => $result['rating'],
                    'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                );
            }

        }


        $data['products_sales'] = array();

        $filter_data = array(
            'filter_category_id' => (int)$sales,
            'start'           => 0,
            'limit'           => 8
        );

        $results = $this->model_catalog_product->getProducts($filter_data);


        if ($results) {
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                    $popup = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'), 'product_list', true);

                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_related_width'), $this->config->get('config_image_related_height'));
                    $popup = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'), 'product_list', true);

                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
                    $wishlist_status = '1';
                } else {
                    $wishlist_status = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $result['rating'];
                } else {
                    $rating = false;
                }

                $data['products_sales'][] = array(
                    'product_id'  => $result['product_id'],
                    'thumb'       => $image,
                    'popup'       => $popup,
                    'name'        => $result['name'],
                    'wishlist_status' => $wishlist_status,
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'tax'         => $tax,
                    'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                    'rating'      => $result['rating'],
                    'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
                );
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/product_home.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/module/product_home.tpl', $data);
            } else {
                return $this->load->view('default/template/module/product_home.tpl', $data);
            }
        }
    }
}
