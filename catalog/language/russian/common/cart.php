<?php
// Text
$_['text_items']     = '%s <span class="hidden">товар(ов) - %s</span>';
$_['text_empty']     = 'В корзине пусто!';
$_['text_cart']      = 'Открыть Корзину';
$_['text_checkout']  = 'Оформить Заказ';
$_['text_recurring'] = 'Профиль платежа';