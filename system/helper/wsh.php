<?php

    if (!function_exists('wsh_log')) {
        function wsh_log($message,$file_name = 'wsh_log'){
            $file = DIR_LOGS . $file_name.'.log';
            $handle = fopen($file, 'a');

            flock($handle, LOCK_EX);

            fwrite($handle, date('Y-m-d G:i:s') . ' - ' .print_r($message, true). "\n");

            fflush($handle);

            flock($handle, LOCK_UN);

            fclose($handle);
        }
    }

?>