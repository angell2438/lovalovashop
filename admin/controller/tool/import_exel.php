<?php
class ControllerToolImportExel extends Controller
{

    public function index()
    {
        $this->load->helper('wsh');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('catalog/manufacturer');
        require_once(DIR_SYSTEM . 'library/PHPExcel/PHPExcel.php');
        //require_once(DIR_SYSTEM . 'library/PHPExcel/PHPExcel/Writer/Excel5.php');
        $excel = PHPExcel_IOFactory::load(DIR_UPLOAD.'import.xlsx');

        $lists = array();
        foreach($excel ->getWorksheetIterator() as $worksheet) {
            $lists[] = $worksheet->toArray();
        }

        $products = array();
        foreach($lists as $kl => $list){
            foreach($list as $kr => $row){
                foreach($row as $kc => $col){
                    $products[$kl][$kr][$kc] = $col;
                    if(($kl == 0 && $kc >= 12)){
                        break;
                    }
                }
                if(($kl == 0 && $kr >= 3)){
                    break;
                }
            }
            if($kl >= 0){
                break;
            }
        }
        $product_name = '';
        $product_model = '';
        wsh_log('import date '.date('now'));
        foreach($products[0] as $key => $product){
            /*
            $product[0] // назва товара
            $product[1] // розширена назва товара (mpn)
            $product[2] // штрихкод (ean)
            $product[3] // артикул (sku)
            $product[4] // код товару (upc)
            $product[5] // країна (jan)
            $product[6] // дата створення
            $product[7] // бренд
            $product[8] // одиниця виміру (isbn)
            $product[9] // ціна
            $product[10]// залишок
            $product[11]// категорія
            $product[12]// категорія +
             */
            if($key>0){
                if(strcmp($product_name,$product[0]) !== 0){
                    $product_model = $product[2];//модель продукта, це перший штрихкод товара, з групи однакових товарів
                    $product_name = trim($product[0]);
                }
                $product_id = $this->model_catalog_product->getProductIdByName(trim($product[0]), trim($product[1]));
                if(isset($product_id['product_id'])){//оновити товар
                    $product_info = $this->model_catalog_product->getProduct($product_id['product_id']);
                    $products_data = array(
                        'image' => $product_info['image'],
                        'model' => $product_info['model'],
                        'newprod' => $product_info['newprod'],
                        'sku' => $product_info['sku'],
                        'upc' => $product_info['upc'],
                        'ean' => $product_info['ean'],
                        'jan' => $product_info['jan'],
                        'isbn' => $product_info['isbn'],
                        'mpn' => $product_info['mpn'],
                        'location' => $product_info['location'],
                        'price' => $product[9],//оновляємо ціну
                        'tax_class_id' => $product_info['tax_class_id'],
                        'quantity' => $product[10],//оновляємо кількість
                        'minimum' => $product_info['minimum'],
                        'subtract' => $product_info['subtract'],
                        'stock_status_id' => $product_info['stock_status_id'],
                        'shipping' => $product_info['shipping'],
                        //'keyword' => $product_info['keyword'],
                        'date_available' => $product_info['date_available'],
                        'length' => $product_info['length'],
                        'width' => $product_info['width'],
                        'height' => $product_info['height'],
                        'length_class_id' => $product_info['length_class_id'],
                        'weight' => $product_info['weight'],
                        'weight_class_id' => $product_info['weight_class_id'],
                        'status' => $product_info['status'],
                        'sort_order' => $product_info['sort_order'],
                        'manufacturer_id' => $product_info['manufacturer_id'],
                        'points' => $product_info['points'],
                    );
                    $product_id = $this->model_catalog_product->updateProduct($product_info['product_id'],$products_data);
                }else{//якщо товара немає додаємо
                    /*Категорії*/
                    $categories = array();
                    $categories_name = array();
                    $categories_name[] = $product[11];
                    if(!empty($product[12])){
                        $categories_name[] = $product[12];
                    }
                    //$categories_name = explode(",", $product[4]);
                    foreach($categories_name as $category){
                        $category_info = $this->model_catalog_category->getCategoryIdByName(trim($category));
                        if(isset($category_info['category_id'])){//якщо в базі є така категорія
                            $categories[] = $category_info['category_id'];
                        }else{
                            $category_data = array (
                                'category_description' => array (
                                    1 => array (
                                        'name' => $category,
                                        'description' => '<p><br></p>',
                                        'meta_title' => '',
                                        'meta_h1' => '',
                                        'meta_description' => '',
                                        'meta_keyword' => '',
                                    ),
                                    3 => array (
                                        'name' => $category,
                                        'description' => '<p><br></p>',
                                        'meta_title' => '',
                                        'meta_h1' => '',
                                        'meta_description' => '',
                                        'meta_keyword' => '',
                                    ),
                                ),
                                'parent_id' => '0',
                                'filter' => '',
                                'category_store' => array (
                                    0 => '0',
                                ),
                                'keyword' => '',
                                'image' => '',
                                'column' => '1',
                                'sort_order' => '0',
                                'status' => '1',
                                'category_layout' => array (
                                    0 => '',
                                ),
                            );
                            $category_id = $this->model_catalog_category->addCategory($category_data);
                            wsh_log('category_id='.$category_id);
                            $categories[] = $category_id;
                        }
                    }
                    $main_category = 0;
                    if($categories){
                        $main_category = $categories[0];
                    }
                    /*Категорії*/

                    /*Виробник*/
                    //$manufacturer_id = 0;
                    $manufacturer_info = $this->model_catalog_manufacturer->getManufacturerIdByName(trim($product[7]));
                    if(isset($manufacturer_info['manufacturer_id'])){//якщо в базі є такий виробник
                        $manufacturer_id = $manufacturer_info['manufacturer_id'];
                    }else{
                        $manufacturer_data = array (
                            'manufacturer_description' => array (
                                1 => array (
                                    'name' => trim($product[7]),
                                    'description' => '<p><br></p>',
                                    'meta_title' => '',
                                    'meta_h1' => '',
                                    'meta_description' => '',
                                    'meta_keyword' => '',
                                ),
                                3 => array (
                                    'name' => trim($product[7]),
                                    'description' => '<p><br></p>',
                                    'meta_title' => '',
                                    'meta_h1' => '',
                                    'meta_description' => '',
                                    'meta_keyword' => '',
                                ),
                            ),
                            'manufacturer_store' => array (
                                0 => '0',
                            ),
                            'keyword' => '',
                            'image' => '',
                            'sort_order' => '',
                        );
                        $manufacturer_id = $this->model_catalog_manufacturer->addManufacturer($manufacturer_data);
                        wsh_log('manufacturer_id='.$manufacturer_id);
                    }
                    /*Виробник*/

                    /*$product_special = array();
                    if($product[7]){
                        $product_special[] = array (
                            'customer_group_id' => '1',
                            'priority' => '0',
                            'price' => (float)$product[6],
                            'date_start' => '',
                            'date_end' => '',
                        );
                        $product[6] = (float)$product[6] * 1.2;
                    }*/
                    $products_data = array(
                        'product_description' => array(
                            1 => array (
                                'name' => trim($product[0]),//Назва товара
                                'description' => '<p><br></p>',
                                'instruction' => '<p><br></p>',
                                'meta_title' => '',
                                'meta_h1' => '',
                                'meta_description' => '',
                                'meta_keyword' => '',
                                'tag' => '',
                            ),
                            3 => array (
                                'name' => trim($product[0]),//Назва товара
                                'description' => '<p><br></p>',
                                'instruction' => '<p><br></p>',
                                'meta_title' => '',
                                'meta_h1' => '',
                                'meta_description' => '',
                                'meta_keyword' => '',
                                'tag' => '',
                            )
                        ),
                        'image' => '',
                        'model' => $product_model,
                        'newprod' => '1',
                        'sku' => $product[3],
                        'upc' => $product[4],
                        'ean' => $product[2],
                        'jan' => $product[5],
                        'isbn' => $product[8],
                        'mpn' => trim($product[1]),
                        'location' => '',
                        'price' => $product[9],
                        'tax_class_id' => '0',
                        'quantity' => $product[10],
                        'minimum' => '1',
                        'subtract' => '1',
                        'stock_status_id' => '5',
                        'shipping' => '1',
                        'keyword' => '',
                        //'date_available' => date("Y-m-d", strtotime(str_replace('\'','',$product[6]))),
                        'date_available' => date("Y-m-d"),
                        'length' => '',
                        'width' => '',
                        'height' => '',
                        'length_class_id' => '1',
                        'weight' => '',
                        'weight_class_id' => '1',
                        'status' => '1',
                        'sort_order' => '1',
                        'manufacturer_id' => $manufacturer_id,
                        'main_category_id' => $main_category,
                        'product_category' => $categories,
                        'filter' => '',
                        'product_store' => array (
                            0 => '0',
                        ),
                        'download' => '',
                        'related' => '',
                        'option' => '',
                        //'product_special' => $product_special,
                        'points' => '',
                        'product_reward' => array (
                            1 => array (
                                'points' => '',
                            ),
                        ),
                        'product_layout' => array (
                            0 => '',
                        ),
                    );
                    $product_id = $this->model_catalog_product->addProduct($products_data);
                    wsh_log('product_id='.$product_id);
                }
                //break;
            }
        }
        //print_r($products_data[0]);
        echo 'complete';
    }

    public function export(){
        $this->load->helper('wsh');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('catalog/manufacturer');

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'pd.name';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'ASC';
        }

        $this->language->load('catalog/product');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');

        $products = array();
        $filter_data = array(
            'sort'            => $sort,
            'order'           => $order
        );

        wsh_log('export date '.date('now'));
        $results = $this->model_catalog_product->getProducts($filter_data);
        /*echo '<pre>';
        print_r($results);
        die();*/
        foreach ($results as $result) {
            /*
            $product[0] // назва товара
            $product[1] // розширена назва товара (mpn)
            $product[2] // штрихкод (ean)
            $product[3] // артикул (sku)
            $product[4] // код товару (upc)
            $product[5] // країна (jan)
            $product[6] // дата створення
            $product[7] // бренд
            $product[8] // одиниця виміру (isbn)
            $product[9] // ціна
            $product[10]// залишок
            $product[11]// категорія
            $product[12]// категорія +
             */
            $categories = array();
            $main_category = '';
            $results_category = $this->model_catalog_product->getProductAllCategories($result['product_id']);
            foreach ($results_category as $category) {//категории продукта
                $category_info = $this->model_catalog_category->getCategory($category['category_id']);
                if($category['main_category']){
                    $main_category = $category_info['name'];
                }else{
                    $categories[] = $category_info['name'];
                }
            }
            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($result['manufacturer_id']);
            //print_r($manufacturer_info);
            $products[] = array(
                'product_id' => $result['product_id'],
                'name'       => $result['name'],    // назва товара
                'mpn'        => $result['mpn'],     // розширена назва товара (mpn)
                'ean'        => $result['ean'],     // штрихкод (ean)
                'sku'        => $result['sku'],     // артикул (sku)
                'upc'        => $result['upc'],     // код товару (upc)
                'jan'        => $result['jan'],     // країна (jan)
                'date_available' => $result['date_available'], // дата створення
                'manufacturer_name' => $manufacturer_info['name'], // бренд
                'isbn'       => $result['isbn'],   // одиниця виміру (isbn)
                'price'      => round($result['price']),// ціна
                'quantity'     => $result['quantity'],// залишок
                'main_category'=> $main_category,//категорія головна
                'category'     => implode(', ',$categories)//категорія
            );

            /*print_r($products);
            break;*/
        }
        require_once(DIR_SYSTEM . 'library/PHPExcel/PHPExcel.php');
        require_once(DIR_SYSTEM . 'library/PHPExcel/PHPExcel/Writer/Excel5.php');
        $styleArray = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN
                )
            )
        );
        $xls = new PHPExcel();
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();

        $sheet->setTitle('Export Product');
        $sheet->getColumnDimension('A')->setWidth(60);
        $sheet->getColumnDimension('B')->setWidth(20);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getColumnDimension('D')->setWidth(10);
        $sheet->getColumnDimension('E')->setWidth(10);
        $sheet->getColumnDimension('F')->setWidth(10);
        $sheet->getColumnDimension('G')->setWidth(10);
        $sheet->getColumnDimension('H')->setWidth(30);
        $sheet->getColumnDimension('I')->setWidth(10);
        $sheet->getColumnDimension('J')->setWidth(10);
        $sheet->getColumnDimension('K')->setWidth(20);
        $sheet->getColumnDimension('L')->setWidth(20);
        $sheet->getColumnDimension('M')->setWidth(60);

        $numRow = 1;
        $sheet->setCellValue("A" . $numRow, '');
        $sheet->getStyle('A' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('A' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('A' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("B" . $numRow, 'Название');
        $sheet->getStyle('B' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('B' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('B' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('B' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("C" . $numRow, 'Штрих-код');
        $sheet->getStyle('C' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('C' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('C' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('C' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("D" . $numRow, 'Артикул');
        $sheet->getStyle('D' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('D' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('D' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('D' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("E" . $numRow, 'Код товара');
        $sheet->getStyle('E' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('E' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('E' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('E' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("F" . $numRow, 'Страна');
        $sheet->getStyle('F' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('F' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('F' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('F' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("G" . $numRow, 'Создан');
        $sheet->getStyle('G' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('G' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('G' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('G' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("H" . $numRow, 'Бренд');
        $sheet->getStyle('H' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('H' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('H' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('H' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("I" . $numRow, 'Ед. изм.');
        $sheet->getStyle('I' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('I' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('I' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('I' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("J" . $numRow, 'Цена');
        $sheet->getStyle('J' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('J' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('J' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('J' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("K" . $numRow, 'Общий остаток');
        $sheet->getStyle('K' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('K' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('K' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('K' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("L" . $numRow, 'Категорія');
        $sheet->getStyle('L' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('L' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('L' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('L' . $numRow)->applyFromArray($styleArray);

        $sheet->setCellValue("M" . $numRow, 'Категорія +');
        $sheet->getStyle('M' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
        $sheet->getStyle('M' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $sheet->getStyle('M' . $numRow)->getAlignment()->setWrapText(true);
        $sheet->getStyle('M' . $numRow)->applyFromArray($styleArray);

        $numRow = 2;
        foreach($products as $key => $product) {
            $sheet->setCellValue("A" . $numRow, $product['name']);
            $sheet->getStyle('A' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('A' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('A' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('A' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("B" . $numRow, $product['mpn']);
            $sheet->getStyle('B' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('B' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('B' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('B' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("C" . $numRow, $product['ean']);
            $sheet->getStyle('C' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('C' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('C' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('C' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("D" . $numRow, $product['sku']);
            $sheet->getStyle('D' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('D' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('D' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('D' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("E" . $numRow, $product['upc']);
            $sheet->getStyle('E' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('E' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('E' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('E' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("F" . $numRow, $product['jan']);
            $sheet->getStyle('F' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('F' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('F' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('F' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("G" . $numRow, $product['date_available']);
            $sheet->getStyle('G' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('G' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('G' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('G' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("H" . $numRow, $product['manufacturer_name']);
            $sheet->getStyle('H' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('H' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('H' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('H' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("I" . $numRow, $product['isbn']);
            $sheet->getStyle('I' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('I' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('I' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('I' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("J" . $numRow, $product['price']);
            $sheet->getStyle('J' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('J' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('J' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('J' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("K" . $numRow, $product['quantity']);
            $sheet->getStyle('K' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('K' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('K' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('K' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("L" . $numRow, $product['main_category']);
            $sheet->getStyle('L' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('L' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('L' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('L' . $numRow)->applyFromArray($styleArray);

            $sheet->setCellValue("M" . $numRow, $product['category']);
            $sheet->getStyle('M' . $numRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $sheet->getStyle('M' . $numRow)->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $sheet->getStyle('M' . $numRow)->getAlignment()->setWrapText(true);
            $sheet->getStyle('M' . $numRow)->applyFromArray($styleArray);

            $numRow++;
        }

        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=export.xls" );

        $objWriter = new PHPExcel_Writer_Excel5($xls);
        $objWriter->save('php://output');
    }

}

?>